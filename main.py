
import torchvision
import torchvision.transforms as transforms

import torch
import torch.nn as nn
import torch.optim as optim
import argparse

from Net import Net

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--lr', type=float, default=0.0001,
                        help='The learning rate.')
    parser.add_argument('--lossLevel', type=float, default=0.510,
                        help='The decrease in loss at which it is time to stop training.')
    parser.add_argument('--iterations', type=int, default=2,
                        help='The number of times to calculate the accuracy for a given parameter set.')
    parser.add_argument('--lrAdder', type=float, default=0.0001,
                        help='The amount to add to the current lr after each iteration.')
    parser.add_argument('--experiments', type=int, default=200,
                        help='The number of experiments to run. Lr is updated after each one.')
    parser.add_argument('--maxEpochs', type=int, default=15,
                        help='The maximum number of epochs to run.')
    parser.add_argument('--momentum', type=float, default=0.0,
                        help='The value to set momentum in the SGD too.')
    parser.add_argument('--print', type=bool, default=False)   
    args = parser.parse_args()
    
    transform = transforms.Compose(
        [transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                            download=True, transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=4,
                                            shuffle=True, num_workers=2)

    testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                        download=True, transform=transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size=4,
                                            shuffle=False, num_workers=2)

    classes = ('plane', 'car', 'bird', 'cat',
            'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    criterion = nn.CrossEntropyLoss()


    lr = args.lr
    for _ in range(args.experiments):
        for _ in range(args.iterations):
            net = Net().to(device)
            optimizer = optim.SGD(net.parameters(), lr=lr, momentum=args.momentum)
            loss = 100
            num_epochs = 0
            while (not hasattr(loss, 'item') or loss.item() > args.lossLevel ) and num_epochs <= args.maxEpochs:
                running_loss = 0.0
                for i, data in enumerate(trainloader, 0):
                    # get the inputs; data is a list of [inputs, labels]
                    inputs, labels = data[0].to(device), data[1].to(device)

                    # zero the parameter gradients
                    optimizer.zero_grad()

                    # forward + backward + optimize
                    outputs = net(inputs)
                    loss = criterion(outputs, labels)
                    # loss.backward()
                    # # compute l2 norm of current gradients
                    # loss.zero_grad()
                    #execute hook
                    loss.backward()
                    optimizer.step()

                    # print statistics
                    if args.print:
                        running_loss += loss.item()
                        if i % 2000 == 1999:    # print every 2000 mini-batches
                             print('[%d, %5d] loss: %.3f' %
                                (num_epochs + 1, i + 1, running_loss / 2000))
                             running_loss = 0.0
                    
                num_epochs += 1


            ########################################################################
            # Let's quickly save our trained model:


            dataiter = iter(testloader)
            images, labels = dataiter.next()
            images = images.to(device)
            labels = labels.to(device)
            outputs = net(images)

            correct = 0
            total = 0
            with torch.no_grad():
                for data in testloader:
                    inputs, labels = data[0].to(device), data[1].to(device)
                    outputs = net(images)
                    _, predicted = torch.max(outputs.data, 1)
                    total += labels.size(0)
                    correct += (predicted == labels).sum().item()

            print(f"\n\nLr: {lr} loss: {loss} Epochs: {num_epochs} Accuracy {100 * correct / total}%\n\n")
            PATH = f"./main:Accuracy:{100 * correct / total}.pth"
            torch.save(net.state_dict(), PATH)
        lr += args.lrAdder
