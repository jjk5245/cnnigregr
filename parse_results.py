import json

file_name='data2.txt'
data = ""
with open(file_name, 'r') as fp:
    for line in fp.readlines():
        if line != '\n':
            data += "{" + line + "},"

json_data = data
json_data =json_data.replace('\n', '')

json_data =json_data.replace(' ', ',')
json_data =json_data.replace(':,', ':')
json_data =json_data.replace('},', '},\n')

json_data =json_data.replace('Lr:', '')
json_data =json_data.replace('lossDiff:', '')
json_data =json_data.replace('Epochs:', '')
json_data =json_data.replace('Accuracy', '')
json_data =json_data.replace('Last,Loss:', '')


json_data =json_data.replace('},', '')
json_data =json_data.replace('{', '')
json_data =json_data.replace(',,', ',')

# json_data = json.loads(json_data)
print("Lr, lossDiff, Epochs, Accuracy, Last Loss\n" + json_data)